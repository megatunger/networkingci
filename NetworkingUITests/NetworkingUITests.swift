//
//  NetworkingUITests.swift
//  NetworkingUITests
//
//  Created by Hoàng Sơn Tùng on 5/20/19.
//  Copyright © 2019 Scott Gardner. All rights reserved.
//

import XCTest

class NetworkingUITests: XCTestCase {

    var app: XCUIApplication!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false

        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app = XCUIApplication()
        app.launchArguments.append("--uitesting")
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func getNumberRepo(RepoName: String) -> Int {
        let search = app.searchFields.element
        search.tap()
        
        search.typeText(RepoName)
        sleep(3)
        
        let table = app.tables
        let countElement = table.cells.count
        
        sleep(1)
        app.buttons["Cancel"].tap()
        return countElement
    }
    
    func testQueryRepos() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        app.launch()
        
        XCTAssert(getNumberRepo(RepoName: "megatunger")==15, "Number repos of megatunger not matched")
        XCTAssert(getNumberRepo(RepoName: "iamvon")==29, "Number repos of megatunger not matched")
    }


}
